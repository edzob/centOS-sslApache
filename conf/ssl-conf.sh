#!/bin/bash

#    This file is created by E A Botjes
#    Copyright (C) 2018 @edzob
#    https://gitlab.com/edzob/centOS-blueApache
#    #####################################################################
#    This file is inspired by blue-team
#    Copyright (C) 2017 @maldevel
#    https://github.com/maldevel/blue-team
#    
#    blue-team - Blue Team Scripts.
#    #####################################################################
#    Other inspiration can be found in the README.md 
#    in the root of the GitLab repository
#    #####################################################################
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#   
#    For more see the file 'LICENSE' for copying permission.

#Set SSL Config
RUN sed -i "/BrowserMatch/i #SSL certificates\n\
            BrowserMatch \"MSIE \[2\-6\]\" nokeepalive ssl\-unclean\-shutdown downgrade-1\.0 force-response\-1\.0\n\
            BrowserMatch \"MSIE \[17\-9\]\" ssl\-unclean\-shutdown\n\
            Header always set Strict-Transport-Security \"max\-age\=31536000\; includeSubDomains\; preload\"\n"\
        /etc/httpd/conf.d/ssl.conf 
RUN sed -i 's/^SSLCipherSuite*/SSLCipherSuite HIGH:!MEDIUM:!aNULL:!MD5:!RC4/g' /etc/httpd/conf.d/ssl.conf
RUN sed -i 's/^SSLProtocol*/SSLProtocol –ALL +TLSv1 +TLSv1.1 +TLSv1.2/g' /etc/httpd/conf.d/ssl.conf
RUN sed -i 's/#SSLHonorCipherOrder On/SSLHonorCipherOrder On/g' /etc/httpd/conf.d/ssl.conf

RUN echo "SSLCompression off" >> /etc/httpd/conf.d/ssl.conf
RUN echo "SSLUseStapling on" >> /etc/httpd/conf.d/ssl.conf
run echo "SSLStaplingCache \"shmcb:logs/stapling-cache(150000)\"" >> /etc/httpd/conf.d/ssl.conf

# Set the keys in SSL Virtual Host
RUN sed -i -- 's/SSLCertificateFile \/etc\/pki\/tls\/certs\/localhost.crt/SSLCertificateFile \/etc\/pki\/tls\/certs\/ca.crt/g' /etc/httpd/conf.d/ssl.conf
RUN sed -i -- 's/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/localhost.key/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/ca.key/g' /etc/httpd/conf.d/ssl.conf



# -----------------------------------------------------------------------------
# Custom Apache configuration
# -----------------------------------------------------------------------------

RUN echo $'\n#\n# Custom configuration\n#' >> $_TARGET \
	&& echo 'Listen 443' >> $_TARGET \
	&& echo 'NameVirtualHost *:80' >> $_TARGET \
	&& echo 'NameVirtualHost *:443' >> $_TARGET \
	&& echo 'Include /etc/httpd/conf/vhost.conf' >> $_TARGET \
	&& echo 'Include /etc/httpd/conf/vhost-ssl.conf' >> $_TARGET \




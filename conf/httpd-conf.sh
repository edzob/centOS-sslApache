#!/bin/bash

#    This file is created by E A Botjes
#    Copyright (C) 2018 @edzob
#    https://gitlab.com/edzob/centOS-blueApache
#    #####################################################################
#    This file is inspired by blue-team
#    Copyright (C) 2017 @maldevel
#    https://github.com/maldevel/blue-team
#    
#    blue-team - Blue Team Scripts.
#    #####################################################################
#    Other inspiration can be found in the README.md 
#    in the root of the GitLab repository
#    #####################################################################
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#   
#    For more see the file 'LICENSE' for copying permission.

# -----------------------------------------------------------------------------
echo "# Apache Environment Variables (for testing without Dockerfile)"
# -----------------------------------------------------------------------------
_APACHE_RUN_USER="apache"
_APACHE_RUN_GROUP="apache"
_APACHE_LOG_DIR="/var/log/httpd"
_APACHE_BASE_CONF_DIR="/etc/httpd/conf"
_APACHE_BASE_CONF_FILENAME="httpd.conf"
_APACHE_EXTRA_CONF_DIR="/etc/httpd/conf.d"
_APACHE_SSL_CONF_FILENAME="ssl.conf"
_APACHE_ServerName="localhost"
_APACHE_CONF_MOD_DIR="/etc/httpd/conf.modules.d"
_TARGET="$_APACHE_BASE_CONF_DIR/$_APACHE_BASE_CONF_FILENAME"

# -----------------------------------------------------------------------------
#echo "# Apache Environment Variables (for testing with Dockerfile)"
# -----------------------------------------------------------------------------
#_TARGET="$APACHE_BASE_CONF_DIR/$APACHE_BASE_CONF_FILENAME"

echo "Target = $_TARGET"

# -----------------------------------------------------------------------------
echo "# Disable all Apache modules"
# -----------------------------------------------------------------------------
sed -i \
	-e 's~^\(LoadModule .*\)$~#\1~g' \
	$_TARGET

for file in $_APACHE_CONF_MOD_DIR/*.conf
do
  mv "$file" "${file/.conf/.conf.old}"
done



# -----------------------------------------------------------------------------
echo "# Enable the minimum Apache modules"
# -----------------------------------------------------------------------------

mv $_APACHE_CONF_MOD_DIR/00-mpm.conf.old $_APACHE_CONF_MOD_DIR/00-mpm.conf
mv $_APACHE_CONF_MOD_DIR/00-base.conf.old $_APACHE_CONF_MOD_DIR/00-base.conf

echo 'LoadModule alias_module modules/mod_alias.so' >> $_TARGET
echo 'LoadModule authz_host_module modules/mod_authz_host.so' >> $_TARGET
echo 'LoadModule deflate_module modules/mod_deflate.so' >> $_TARGET
echo 'LoadModule dir_module modules/mod_dir.so' >> $_TARGET
echo 'LoadModule expires_module modules/mod_expires.so' >> $_TARGET
echo 'LoadModule headers_module modules/mod_headers.so' >> $_TARGET
echo 'LoadModule log_config_module modules/mod_log_config.so' >> $_TARGET
echo 'LoadModule mime_magic_module modules/mod_mime_magic.so' >> $_TARGET
echo 'LoadModule setenvif_module modules/mod_setenvif.so' >> $_TARGET
echo 'LoadModule status_module modules/mod_status.so' >> $_TARGET

# -----------------------------------------------------------------------------
echo "# Global Apache configuration changes"
# -----------------------------------------------------------------------------
echo "ServerName localhost" >> $_TARGET
sed -i \
	-e 's~^ServerSignature On\(.*\)$~#ServerSignature On\1~g' \
	-e 's~^ServerTokens OS\(.*\)$~#ServerTokens OS\1~g' \
	-e 's~^ExtendedStatus On\(.*\)$~#ExtendedStatus On\1~g' \
	-e 's~^TraceEnable On\(.*\)$~#TraceEnable On\1~g' \
	$_TARGET
echo "ServerSignature Off"  >> $_TARGET
echo "ServerTokens Prod"    >> $_TARGET
echo "ExtendedStatus Off"  >> $_TARGET
echo "TraceEnable off" >> $_TARGET


# -----------------------------------------------------------------------------
echo "# DDOS: Default Apache Timeout is 300 seconds set to 60"
# -----------------------------------------------------------------------------
echo "Timeout 60" >> $_TARGET
echo "LimitRequestBody 1024000" >> $_TARGET

# -----------------------------------------------------------------------------
echo "# Disable HTTP 1.0 Protocol"
# -----------------------------------------------------------------------------
echo "RewriteEngine On" >> $_TARGET
echo "RewriteCond %{THE_REQUEST} !HTTP/1.1$" >> $_TARGET
echo "RewriteRule .* - [F]" >> $_TARGET

# -----------------------------------------------------------------------------
echo "# Apache Header configuration"
# -----------------------------------------------------------------------------
############ Enable HttpOnly and Secure flags
echo 'Header edit Set-Cookie ^(.*)$ $1;HttpOnly;Secure' >> $_TARGET

############ Clickjacking Attack Protection
echo 'Header always append X-Frame-Options SAMEORIGIN' >> $_TARGET

############# XSS Protection
echo 'Header set X-XSS-Protection "1; mode=block"' >> $_TARGET

############ Enforce secure connections to the server
echo "Header always set Strict-Transport-Security \"max-age=31536000; includeSubDomains\"" >> $_TARGET

############ MIME sniffing Protection
echo "Header set X-Content-Type-Options: \"nosniff\"" >> $_TARGET

############ Prevent Cross-site scripting and injections via Content Security Policy (CSP)
echo "Header set Content-Security-Policy \"default-src 'self';\"" >> $_TARGET
echo "Header set Content-Security-Policy \"default-src 'self'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self'; frame-ancestors 'self'\"" >> $_TARGET

############ Remove ID for resources (PCI Compliancy)
echo "FileETag None" >> $_TARGET
echo "Header unset Etag" >> $_TARGET

############ HTTP Public Key Pins (HPKP)
echo 'Header always set Public-Key-Pins "pin-sha256=\"base64+primary==\"; pin-sha256=\"base64+backup==\"; max-age=5184000; includeSubDomains"' >> $_TARGET

# -----------------------------------------------------------------------------
echo "# Add logformat fields"
# -----------------------------------------------------------------------------
sed -i \
	-e "/'<IfModule log_config_module>'/a 'LogFormat \"%h %l %u %t \"%{sessionID}C\" \"%r\" %>s %b %T\" common'" \
	$_TARGET

# -----------------------------------------------------------------------------
echo "# Disable Apache directory indexes"
# -----------------------------------------------------------------------------
sed -i \
	-e 's~^IndexOptions \(.*\)$~#IndexOptions \1~g' \
	-e 's~^IndexIgnore \(.*\)$~#IndexIgnore \1~g'  \
	$_TARGET
sed -i \
    -e "s/Options\ Indexes\ FollowSymLinks/Options\ -Indexes\ +FollowSymLinks/g" \
    $_TARGET

# -----------------------------------------------------------------------------
echo "# Disable Apache Server Side Includes"
# -----------------------------------------------------------------------------
sed -i \
    -e "s/\ Includes\ /\ -Includes\ /g" \
    $_TARGET

# -----------------------------------------------------------------------------
echo "# Disable Apache directory icons"
# -----------------------------------------------------------------------------	
sed -i \
	-e 's~^AddIconByEncoding \(.*\)$~#AddIconByEncoding \1~g' \
	-e 's~^AddIconByType \(.*\)$~#AddIconByType \1~g' \
	-e 's~^AddIcon \(.*\)$~#AddIcon \1~g' \
	-e 's~^DefaultIcon \(.*\)$~#DefaultIcon \1~g' \
	$_TARGET

# -----------------------------------------------------------------------------
echo "# Disable Apache directory files"
# -----------------------------------------------------------------------------	
sed -i \
	-e 's~^ReadmeName \(.*\)$~#ReadmeName \1~g' \
	-e 's~^HeaderName \(.*\)$~#HeaderName \1~g' \
	$_TARGET

# -----------------------------------------------------------------------------
echo "# Apache Configtest"
# -----------------------------------------------------------------------------
apachectl configtest
# CentOS SSLapache
This repository is a simple centOS container.   
* CentOS Base install 
* updated via yum update.
* Apache (only) installed.
* Apache mod_ssl installed
* Basic keys installed.

**Table of Contents** 
- [CentOS SSLapache](#centOS-sslapache)
- [todo](#todo)
- [TL;DR](#tl;dr)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Run it](#run-it)
- [Built With](#built-with)
  - [Devices](#devices)
  - [CodeEditor](#codeeditor)
  - [Programming Language(s)](#programming-language(s))
  - [Tools](#tools)
- [Contributing](#contributing)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)
  - [Inspiration on Readme.md](#inspiration-on-readme.md)
  - [Inspiration on Markdown Syntax](#inspiration-on-markdown syntax)

## todo
* ...

## TL;DR
`docker-compose build`

`docker-compose up`

`lynx localhost:80`
`lynx localhost:80/test` 

`docker-compose down`

## On Apache-SSL
### Goal
1. The goal is to learn from the steps and fiddle around,
  1. thus during container creation the certificates are created.

1. The goal is to create a server that  
  1. is default less vurnable for attacks and 
  1. has SSL support.

1. This is more work then 
  1. setup a server with Letsencrypt,
  1. setup a server with certificates that you already have.

1. This container is part of the the following series:
  1. centOS updated
  1. centoS Apache
  1. centOS Apache Hardnend





## Getting Started
This code will give you a docker container with the most recent CentOS, updated, apache base installed and configuration of apache changed by a script.

### Prerequisites
see Gitlab [centOS-Updated](https://gitlab.com/edzob/centOS-updated) for instructions and documentation
1. You need a machine with docker and it would help to also have docker-compose installed.
  1. Select place for running docker daemon
  1. Setup VM (gitlab-keys & docker & docker-compose)

### Run it
1. Login on your vm
1. go to the project dir for example 
`cd Projects`
1. clone the project repository 
`git clone https://gitlab.com/edzob/centOS-SSLapache` 
1. go to the repository dir 
`cd centOs-SSLapache`
1. build the container via docker compose. 
`docker-compose build`
1. run the container (spin up)
`docker-compose up`
1. Validate the Apache is running
`http://external-ip:80` 
1. Validate that the script has run successfull
`http://external-ip:80/test`    
1. Login to the container and look around.
`docker-compose run --rm centos-SSLapache /bin/bash`


## Built With
### Devices
* Chromebook
* Google Cloud Computing

### CodeEditor
* Chromebook
    * [Cloud9](https://ide.c9.io/) Code editor
* Google cloud
    * vi

### Programming Language(s)
* Bash
* Dockerfile
* YML

### Tools
* [Docker](https://docs.docker.com)
* [Docker-compose](https://docs.docker.com/compose)
* [Lynx](http://lynx.browser.org/)
    * Alternate : [elinks](http://elinks.or.cz/) 

## Contributing

Please read [CONTRIBUTING.md] for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Edzo Botjes** - *Initial work* - [Gitlab](https://gitlab.com/edzob), [Blogger](https://blog.edzob.com/), [Medium](https://medium.com/@edzob), [LinkedIN](https://www.linkedin.com/in/edzob/), [twitter](https://twitter.com/edzob) 

See also the list of [contributors](https://gitlab.com/edzob/centOS-sslApache/graphs/master) who participated in this project.

## License
```
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#   
#    For more see the file 'LICENSE' for copying permission.
```

## Acknowledgments

### Inspiration on SSL
* Why [1][ssl01] [nixCraft - CentOS Redhat Apache mod_ssl Configuration][ssl01]
  * Step 1: Create an SSL Certificate
      * `# cd /etc/pki/tls/certs`
      * `# openssl genrsa -des3 -out apachekey.pem 2048`
  * Step 2: GENERATE A CERTIFICATE SIGNING REQUEST (CSR)
      * `# openssl req -new -key apachekey.pem -out apachekey.csr`
  * Step 3: Create the Web Server Certificate
      * `openssl ca -in apachekey.csr -out apachecert.pem` 
  * Step 4: Install SSL Certificate
      * `# cp apachecert.pem /etc/pki/tls/http/`
      * `# cp apachekey.pem /etc/pki/tls/http/`
      * `vi /etc/httpd/conf.d/ssl.conf`
      * `# mkdir -p /var/www/html/ssl`
      * `# vi /etc/httpd/conf/httpd.conf`
      * `# service httpd restart`

* [thegeekdiary - CentOS / RHEL : How to Enable SSL For Apache][ssl02]
  * Step 1: Edit SSL Certificate And Keys
      * `# vi /etc/httpd/conf.d/ssl.conf`
      * `# ServerName www.example.com:443`
      * `SSLCertificateFile /etc/pki/tls/certs/localhost.crt`
      * `SSLCertificateKeyFile /etc/pki/tls/private/localhost.key`
  * Step 2: Restart the Apache webserver

* [Redhat 5 - 25.8. Apache HTTP Secure Server Configuration][ssl03] - [Redhat - Types of Certificates][ssl03b]
  * The process of getting a certificate from a CA
      * Step 1: Create an encryption private and public key pair.
          * `cd /etc/pki/tls/`
          * `rm private/server.key`
          * `rm certs/server.crt`
          * `genkey www.example.com`
      * Step 2: Create a certificate request based on the public key. 
          * The certificate request contains information about your server and the company hosting it.
      * Step 3: Send the certificate request, along with documents proving your identity, to a CA. 
          * Red Hat does not make recommendations on which certificate authority to choose. 
          * Your decision may be based on your past experiences, on the experiences of your friends or colleagues, or purely on monetary factors.
          * Once you have decided upon a CA, you need to follow the instructions they provide on how to obtain a certificate from them.
      * Step 4: When the CA is satisfied that you are indeed who you claim to be, they provide you with a digital certificate.
      * Step 5: Install this certificate on your secure server and begin handling secure transactions.

* [StackeExchange - How to configure SSL in apache?][ssl04]
  * Step 1: Create keys
    * `openssl genrsa -out mydomain.key 1024` 
  * Step 2: Generate certificate
    * `openssl req -new -key mydomain.key -x509 -out mydomain.crt` 
  * Step 3: Organize files
    * Keep the private key in the directory /etc/apache2/ssl.key/  
    * Keep certificate in the directory /etc/apache2/ssl.crt/ 
  * Step 4: edit apache configuration
    * Now you need to edit httpd.conf file in /etc/apache2
    * Don't change Listen 80 to 443 in /etc/httpd/conf/httpd.conf. 
    * SSL is configured in /etc/httpd/conf.d/ssl.conf. 

* [ShellHacks - OpenSSL: Check SSL Certificate Expiration Date and More][ssl05]
  * Loads of SSL Debug tips

* [ShellHacks - How To: Create Self-Signed Certificate – OpenSSL][ssl05b]
  * Create Self-Signed Certificate
  * `$ openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -nodes -days 365 -subj '/CN=localhost'`
 
* [ShellHacks - HowTo: Create CSR using OpenSSL Without Prompt (Non-Interactive)][ssl05c]
* Create CSR and Key Without Prompt using OpenSSL
  * `$ openssl req -nodes -newkey rsa:2048 -keyout example.key -out example.csr -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=example.com"`
* Generate CSR From the Existing Key using OpenSSL
  * `$ openssl req -new -key example.key -out example.csr -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=example.com"`
* Automated Non-Interactive CSR Generation
  * The magic of CSR generation without being prompted for values which go in the certificate’s subject field, is in the -subj option. 

* [ServerVault - What Should be the Permissions of Apache SSL Directory, Certificate, and Key?][ssl06]
  * root.root 700 /etc/apache2/ssl 
  * root.root 600 /etc/apache2/ssl/cert.pem
  * root.root 600 /etc/apache2/ssl/cert.key

* [DigitalOcean -  How To Create a Self-Signed SSL Certificate for Apache in Ubuntu 16.04][ssl07]
  * Step 1: Create the SSL Certificate
    * `sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt`
    * `sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048`
    * Fill out the prompts appropriately. The most important line is the one that requests the Common Name (e.g. server FQDN or YOUR name). You need to enter the domain name associated with your server or, more likely, your server's public IP address.
  * Step 2: Configure Apache to Use SSL
    * Step 2.1: We will create a configuration snippet to specify strong default SSL settings.
      * `sudo nano /etc/apache2/conf-available/ssl-params.conf`
    * Step 2.2: We will modify the included SSL Apache Virtual Host file to point to our generated SSL certificates.
      * `sudo cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf.bak`
      * `sudo nano /etc/apache2/sites-available/default-ssl.conf`
    * Step 2.3: (Recommended) We will modify the unencrypted Virtual Host file to automatically redirect requests to the encrypted Virtual Host.
      * `sudo nano /etc/apache2/sites-available/000-default.conf`
  * Step 3: Adjust the Firewall
  * Step 4: Enable the Changes in Apache
  * Step 5: Test Encryption
  * Step 6: Change to a Permanent Redirect
 
* [Suyash Kumar / Jenkins SSL + Docker / gen_certs.sh][ssl08]
```
openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout server.key \
    -new \
    -out server.pem \
    -subj /CN=localhost \
    -reqexts SAN \
    -extensions SAN \
    -config <(cat $sslConfig \
        <(printf '[SAN]\nsubjectAltName=DNS:localhost')) \
    -sha256 \
    -days 3650
```

* [Docker - Applications with SSL][ssl09]
* You can generate a self-signed certificate for app.example.org by running:
``` 
openssl req \
  -new \
  -newkey rsa:4096 \
  -days 3650 \
  -nodes \
  -x509 \
  -subj "/C=US/ST=CA/L=SF/O=Docker-demo/CN=app.example.org" \
  -keyout app.example.org.key \
  -out app.example.org.cert
```

* [StackOverflow - Enable Apache SSL in Docker for local development][ssl10]
  * Besides enabling ssl and exposing port 443, you need to create a (self-signed) certificate + private key and make sure Apache has access to those.
  * openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj \
    "/C=../ST=...../L=..../O=..../CN=..." \
    -keyout ./ssl.key -out ./ssl.crt
  * Instead of the dots (...) fill in your 2-letter country code (/C), the name of your state or province (/ST), the name of your locality (/L), the name of your organization (/O) and your server FQDN (/CN)
  * 

* [Shiu You-sheng - How to create self-assigned SSL for local docker-based LAMP dev environment on macOS Sierra][ssl11]
  * Step 1: Create your own key and certificate by openssl
    * `openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout server.key -out server.crt`
    * “Common Name” : Must be the domain name you are going to secure.
    * `Common Name (e.g. server FQDN or YOUR name) []: dev`
  * Step 2: Write your virtual host configuration file
    * `vi /var/www/html/dev.conf`
    * /etc/apache2/ssl is where we put key files
  * Step 3: Put all generated files into docker image
    * COPY server.crt /etc/apache2/ssl/server.crt
    * COPY server.key /etc/apache2/ssl/server.key
    * COPY dev.conf /etc/apache2/sites-enabled/dev.conf
  * Step 4: Build and run your docker image
  * Step 5: Add your your certificate into keychanin as a trusted source (macOS).
    * `sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain server.crt`
* [Docker very secured Apache2 with secure SSL (marvambass/apache2-ssl-secure)][ssl12]
  * Creating a high secure SSL CSR with openssl
    * `openssl req -nodes -new -newkey rsa:4096 -out csr.pem -sha256`
  * Creating a self-signed ssl cert
    * `openssl req -x509 -newkey rsa:4086 -keyout key.pem -out cert.pem -days 3650 -nodes -sha256`

* [nezhar php-docker-ssl][ssl13]
  * `openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/ssl-cert-snakeoil.key -out /etc/ssl/certs/ssl-cert-snakeoil.pem -subj "/C=AT/ST=Vienna/L=Vienna/O=Security/OU=Development/CN=example.com"`

[ssl01]: https://www.cyberciti.biz/faq/rhel-apache-httpd-mod-ssl-tutorial
[ssl02]: https://www.thegeekdiary.com/centos-rhel-how-to-enable-ssl-for-apache
[ssl03]: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/5/html/deployment_guide/s1-httpd-secure-server
[ssl03b]: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/5/html/deployment_guide/s1-httpd-secure-server#s2-secureserver-certs
[ssl04]: https://unix.stackexchange.com/questions/36786/how-to-configure-ssl-in-apache
[ssl05]: https://www.shellhacks.com/openssl-check-ssl-certificate-expiration-date/
[ssl05b]: https://www.shellhacks.com/create-self-signed-certificate-openssl/
[ssl05c]: https://www.shellhacks.com/create-csr-openssl-without-prompt-non-interactive/
[ssl06]: https://serverfault.com/questions/216477/what-should-be-the-permissions-of-apache-ssl-directory-certificate-and-key
[ssl07]: https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-16-04
[ssl08]: https://github.com/suyashkumar/jenkins-ssl-docker/blob/master/gen_certs.sh
[ssl09]: https://docs.docker.com/ee/ucp/interlock/usage/tls/#let-your-service-handle-tls
[ssl10]: https://stackoverflow.com/questions/43752615/enable-apache-ssl-in-docker-for-local-development
[ssl11]: https://medium.com/@nh3500/how-to-create-self-assigned-ssl-for-local-docker-based-lamp-dev-environment-on-macos-sierra-ab606a27ba8a
[ssl12]: https://github.com/MarvAmBass/docker-apache2-ssl-secure
[ssl13]: https://github.com/nezhar/php-docker-ssl/blob/master/Dockerfile

### Inspiration on Readme.md
* Inspiration on Readme.md
  * Github user PurpleBooth wrote: "[A template to make good README.md][readme01]"
  * Github user jxson wrote: "[README.md template][readme02]"
  * Medium user meakaakka wrote: "[A Beginners Guide to writing a Kickass README][readme03]"

### Inspiration on Markdown Syntax
* Inspiration on Markdown Syntax
    * GitHub   
      * Github user __adam-p__ wrote: "[Markdown Cheatsheet][markdown01]"
      * __Github Guides__ wrote: "[Markdown Syntax (pdf)][markdown02]"
      * __Github Guides__ wrote: "[Mastering Markdown (Wiki)][markdown03]"
      * Github user __tchapi__ wrote: "[Markdown Cheatsheet for Github][markdown04]"
    * GitLab
      * __GitLab documentation__ - Markdown wrote: "[GitLab Flavored Markdown (GFM)][markdown05]"
      * __GitLab Team Handbook__ - Markdown Guide wrote: "[Markdown kramdown Style Guide for about.GitLab.com][markdown06]"

[readme01]: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
[readme02]: https://gist.github.com/jxson/1784669
[readme03]: https://medium.com/@meakaakka/a-beginners-guide-to-writing-a-kickass-readme-7ac01da88ab3

[markdown01]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
[markdown02]: https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf
[markdown03]: https://guides.github.com/features/mastering-markdown/ 
[markdown04]: https://github.com/tchapi/markdown-cheatsheet

[markdown05]: https://docs.gitlab.com/ee/user/markdown.html
[markdown06]: https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/
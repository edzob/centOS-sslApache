# Pull from CentOS RPM Build Image
FROM centos

MAINTAINER Edzo Botjes <e.a.botjes@gmail.com>

# Apache Environment Variables
ENV APACHE_DOC_ROOT "/var/www/html"
ENV APACHE_RUN_USER "apache"
ENV APACHE_RUN_GROUP "apache"
ENV APACHE_LOG_DIR "/var/log/httpd"
ENV APACHE_BASE_CONF_DIR "/etc/httpd/conf"
ENV APACHE_BASE_CONF_FILENAME "httpd.conf"
ENV APACHE_EXTRA_CONF_DIR "/etc/httpd/conf.d"
ENV APACHE_SSL_CONF_FILENAME "ssl.conf"
ENV APACHE_ServerName "localhost"

# SSL Cert Environment Variables
ENV c "NL"
ENV st "Hilversum"
ENV l "North Holland"
ENV o "Test Company"
ENV ou "Example Excersise"
ENV cn "John Doe"

# security.txt Enviroment Variables
ENV SECURITY_EMAIL "security@example.com"
ENV SECURITY_PGP_PUBLIC_KEY "https://example.com/pgp-key.txt"
ENV SECURITY_POLICY "https://example.com/security-policy.html"

# Update the image
RUN yum install --setopt=tsflags=nodocs -y deltarpm && \
    rpm --rebuilddb && yum clean all && \
    yum install --setopt=tsflags=nodocs -y epel-release && \
    yum update --setopt=tsflags=nodocs -y

# Use easy install to install supervisord
RUN yum --setopt=tsflags=nodocs -y install python-setuptools
RUN easy_install supervisor
RUN mkdir -p /var/log/supervisor
COPY conf/supervisord.conf /etc/supervisord.conf

# Some tool for when needed.
# RUN yum --setopt=tsflags=nodocs -y install which vi sed git lynx

##############################
## Setting up Apache
##############################

# Install Apache + SSL
RUN yum --setopt=tsflags=nodocs -y install httpd
RUN yum --setopt=tsflags=nodocs -y install mod_ssl openssl

# Get hardening script and run it?
COPY conf/httpd-conf.sh /tmp/httpd-conf.sh
 RUN chmod 500 /tmp/httpd-conf.sh 
 RUN /tmp/httpd-conf.sh

# robots.txt
RUN echo "User-agent: *" > $APACHE_DOC_ROOT/robots.txt && \
    echo "Disallow: /" >> $APACHE_DOC_ROOT/robots.txt

# security.txt
RUN echo "# Our security address" > $APACHE_DOC_ROOT/security.txt && \
    echo "Contact: mailto:$SECURITY_EMAIL" >> $APACHE_DOC_ROOT/security.txt && \
    echo "">> $APACHE_DOC_ROOT/security.txt && \
    echo "# Our PGP key" >> $APACHE_DOC_ROOT/security.txt && \
    echo "Encryption: $SECURITY_PGP_PUBLIC_KEY" >> $APACHE_DOC_ROOT/security.txt && \
    echo "" >> $APACHE_DOC_ROOT/security.txt && \
    echo "# Our security policy" >> $APACHE_DOC_ROOT/security.txt && \
    echo "Policy: $SECURITY_POLICY" >> $APACHE_DOC_ROOT/security.txt
    
# Content WebServer
RUN echo "<p>Hello Apache server on CentOS Docker </p>" >  $APACHE_DOC_ROOT/index.html && \
    echo "<p>$(cat /etc/centos-release) </p>" >> $APACHE_DOC_ROOT/index.html && \
    echo "<p>$(apachectl -v) </p>" >> $APACHE_DOC_ROOT/index.html
RUN mkdir  $APACHE_DOC_ROOT/test -p && touch  $APACHE_DOC_ROOT//test/hi && touch  $APACHE_DOC_ROOT//test/hello 

##############################
## Setting up SSL - Keys
##############################

# Generate keys
RUN mkdir /etc/ssl/private -p && \ 
     chmod 700 /etc/ssl/private && \
     openssl genrsa -out /etc/ssl/private/ca.key 2048 && \
     openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048 
 
# create Certificate Signing Request
RUN openssl req 
    -new 
    -key /etc/ssl/private/ca.key 
    -out /etc/ssl/certs/ca.csr  
    -subj "/C=$c/ST=$st/L=$l/O=$o/OU=$ou/CN=$cn"
#
# Self Sign Certificate Request
RUN openssl x509 -req 
    -days 365 
    -in /etc/ssl/certs/ca.csr 
    -signkey /etc/ssl/private/ca.key 
    -out /etc/ssl/certs/ca.crt
# 
# add forward secrecy (dhparam) to self signed certificate
RUN cat /etc/ssl/certs/dhparam.pem | tee -a /etc/ssl/certs/ca.crt
# 
# Copy the files to the correct locations
RUN cp /etc/ssl/private/ca.key /etc/pki/tls/private/.


#cleanup
RUN yum clean all && rm -rf /var/cache/yum

EXPOSE 80 
CMD ["/usr/bin/supervisord"]